// TODO/ASG:
// - creation d'une classe Horloge
// - mettre des COMMENTAIRES!!!!!!!!!!!!!!!!!!!!


const formatDate = 'DD.MM.YYYY'; // format d'affichage de la date
const formatHeure = 'HH:mm:ss'; // format d'affichage de l'heure

let handleDate; // reference au setTimeout qui pilote la date
let dateActuelle; // date du jour
let handleHeure; // reference au setInterval qui pilote l'heure
let heureActuelle; // heure du moment

// calcule le decalage entre maintenant et le prochain changement de date.
function calculeProchaineDate(dateJour) {
  let tsJourActuel; // timestamp de la date du jour
  let tsDemainMinuit; // timestamp du lendemain a minuit

  tsJourActuel = moment(dateJour);
  tsDemainMinuit = moment(dateJour).add(1, 'days').startOf('day');
  return tsDemainMinuit - tsJourActuel;
};

// calcule le decalage entre maintenant et le prochain changement de seconde.
function calculeProchaineSeconde(heureJour) {
  let tsHeureActuelle; // timestamp de l'heure actuelle
  let tsProchaineSeconde; // timestamp de la prochaine seconde

  tsHeureActuelle = moment(heureJour);
  tsProchaineSeconde = moment(heureJour).add(1, 'seconds').milliseconds(0);
  return tsProchaineSeconde - tsHeureActuelle;
};

// Initialise le premier declenchement de l'affichage de la date et de l'heure.
// En effet, afin d'eviter de scruter chaque seconde ou minute un changement de date,
// il est preferable d'initialiser le prochain evenement de changement de date.
// A une echelle moindre, le meme probleme se pose avec l'heure.
function initialisationDateHeure() {
    // on calcule le nombre de milli-secondes avant le prochain changement de date.
    dateActuelle = new Date();
    handleDate = setTimeout(gestionDate, calculeProchaineDate(dateActuelle));
    // on calcule le nombre de milli-secondes avant le prochain changement de seconde.
    heureActuelle = new Date();
    handleHeure = setTimeout(gestionHeure, calculeProchaineSeconde(heureActuelle));
};

// affiche la date, programme le prochain affichage.
function gestionDate() {
    clearTimeout(handleDate);
    dateActuelle = new Date();
    handleDate = setTimeout(gestionDate, calculeProchaineDate(dateActuelle));
    affichage_de_la_date();
    affichage_du_jour_dans_lAnnee();
};

// affiche l'heure, programme le prochain affichage.
function gestionHeure() {
    clearTimeout(handleHeure);
    heureActuelle = new Date();
    handleHeure = setTimeout(gestionHeure, calculeProchaineSeconde(heureActuelle));
    affichage_de_l_heure();
};

function affichage_de_l_heure() {
  $(document).find('.heure_actuelle').html('<strong>' + moment(heureActuelle).format(formatHeure) + '</strong>');
};

function affichage_de_la_date() {
  $(document).find('.date_actuelle').html('<strong>' + moment(dateActuelle).format(formatDate) + '</strong>');
};

function affichage_du_jour_dans_lAnnee() {
  let jour;

  jour = moment(dateActuelle).dayOfYear();
  $(document).find('.jourDelAnnee').text(jour + (1 === jour? 'er' : 'éme'));
};

$(document).ready(function() {
    initialisationDateHeure();
    affichage_de_la_date();
    affichage_de_l_heure();
    affichage_du_jour_dans_lAnnee();
} );
